using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareAndVolume : MonoBehaviour
{
    public static SquareAndVolume instance;
    public float spaceVolume;
    public List<float> wallSquares;

    private void Start()
    {
        instance = this;
    }
    /// <summary>
    /// Method for calculating space volume 
    /// V = S * H
    /// </summary>
    /// <param name="perimetrPointsCoord"></param>
    /// <param name="heightDist"></param>
    public void CalculateSpaceVolume(List<Vector3> perimetrPointsCoord, float heightDist)
    {
        int n = perimetrPointsCoord.Count;
        float p_sum=0, m_sum=0;
        for(int i = 0; i < n - 1; i++)
        {
            p_sum += perimetrPointsCoord[i].x * perimetrPointsCoord[i + 1].y;
        }
        p_sum += perimetrPointsCoord[n - 1].x * perimetrPointsCoord[1].y;
        for (int i = 0; i < n - 1; i++)
        {
            m_sum += perimetrPointsCoord[i+1].x * perimetrPointsCoord[i].y;
        }
        m_sum -= perimetrPointsCoord[1].x * perimetrPointsCoord[n-1].y;
        Debug.Log($"Base square is {Mathf.Abs((p_sum - m_sum) / 2)}");   
        Debug.Log($"Height is {heightDist}");
        spaceVolume = Mathf.Abs((p_sum - m_sum) / 2) * heightDist;
        Debug.Log($"Space Volume is {spaceVolume}");
    }
    /// <summary>
    /// Methon for calculating square of each wall
    /// </summary>
    /// <param name="perimetrPointsCoord"></param>
    /// <param name="heightDist"></param>
    public void WallSquares(List<Vector3> perimetrPointsCoord, float heightDist)
    {
        int n = perimetrPointsCoord.Count;
        for (int i = 0;i < n; i++)
        {
            float distance;
            if (i == (n - 1))
            {
                distance = Vector3.Distance(perimetrPointsCoord[0], perimetrPointsCoord[n - 1]);
            }
            else
            {
                distance = Vector3.Distance(perimetrPointsCoord[i], perimetrPointsCoord[i + 1]);
            }
            wallSquares.Add(distance * heightDist);
            Debug.Log($"Wall {i + 1} has square {wallSquares[i]}");
        }
    }
}
