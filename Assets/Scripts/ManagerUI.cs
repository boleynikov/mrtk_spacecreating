using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ManagerUI : MonoBehaviour
{
    [SerializeField] GameObject DeleteLastButton;
    [SerializeField] GameObject ContinueButton;
    [SerializeField] GameObject OkButton;
    [SerializeField] GameObject ShowInfoButton;
    [SerializeField] GameObject firstMessage;
    [SerializeField] GameObject secondMessage;
    [SerializeField] GameObject infoMessage;
    bool isFirstMessage, isSecondMessage;
    bool infoVisible;
    public static ManagerUI instance;

    void Start()
    {
        instance = this;
        StartProperties();
    }
    /// <summary>
    /// Method with start UI interface properties
    /// </summary>
    void StartProperties()
    {
        firstMessage.SetActive(true);
        secondMessage.SetActive(false);
        infoMessage.SetActive(false);
        DeleteLastButton.SetActive(false);
        ContinueButton.SetActive(false);
        ShowInfoButton.SetActive(false);
        OkButton.SetActive(true);
        isFirstMessage = true;
        isSecondMessage = false;
        infoVisible = false;
    }
    /// <summary>
    /// Restart button clicked
    /// </summary>
    public void RestartClick()
    {
        Main.instance.isDraw = false;
        Main.instance.Restart();
        Poly.instance.DeleteSurfaces();
        Poly.instance.DeleteTooltips();
        StartProperties();
        Debug.Log("RestartBtn");
    }
    /// <summary>
    /// DeleteLastPoint button clicked
    /// </summary>
    public void DeleteLastClick()
    {
        Main.instance.DeleteLastPoint();
        Debug.Log("DeleteLastPointBtn");
    }
    /// <summary>
    /// Continue button clicked
    /// </summary>
    public void ContinueClick()
    {
        if (isFirstMessage && Main.instance.perimetrPointsCoord.Count >= 2)
        {
            Main.instance.isDraw = false;
            isFirstMessage = false;
            isSecondMessage = true;
            firstMessage.SetActive(false);
            secondMessage.SetActive(true);
            DeleteLastButton.SetActive(false);
            ContinueButton.SetActive(false);
            OkButton.SetActive(true);
        }
        else if (isSecondMessage)
        {
            Main.instance.isDraw = false;
            isSecondMessage = false;
            secondMessage.SetActive(false);
            DeleteLastButton.SetActive(false);
            ContinueButton.SetActive(false);
            OkButton.SetActive(false);
            Main.instance.Calculate();
            Main.instance.SortPoints();
            Poly.instance.CreateSurfaces(Main.instance.perimetrPointsCoord, Main.instance.heightDist);
            SquareAndVolume.instance.CalculateSpaceVolume(Main.instance.perimetrPointsCoord, Main.instance.heightDist);
            SquareAndVolume.instance.WallSquares(Main.instance.perimetrPointsCoord, Main.instance.heightDist);
            ShowInfoButton.SetActive(true);
            infoMessage.SetActive(true);
        }
        Debug.Log("ContinueBtn");
    }
    /// <summary>
    /// Ok button clicked
    /// </summary>
    public void OkClick()
    {
        if (isFirstMessage)
        {          
            Main.instance.InputPerimetrPoints();
        }
        if (isSecondMessage)
        {          
            Main.instance.InputHeightPoints(); 
        }
        Main.instance.isDraw = true;
        DeleteLastButton.SetActive(true);
        ContinueButton.SetActive(true);
        OkButton.SetActive(false);
        Debug.Log("OkBtn");
    }
    /// <summary>
    /// ShowInfo button clicked
    /// </summary>
    public void ShowSpaceInfo()
    {
        if (infoVisible)
        {
            Poly.instance.DeleteTooltips();
            ShowInfoButton.GetComponent<ButtonConfigHelper>().MainLabelText = "Show space info";
            infoVisible = false;
        }
        else 
        {
            Poly.instance.CreateTooltips(Main.instance.DrawnHeights);
            infoMessage.GetComponent<TextMeshProUGUI>().text = $"Space volume is {SquareAndVolume.instance.spaceVolume} qbm.";
            ShowInfoButton.GetComponent<ButtonConfigHelper>().MainLabelText = "Hide space info";
            infoVisible = true;
        }
    }
}
