using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector3AndDistance : MonoBehaviour
{
    public Vector3 point;
    public float distance;
    public Vector3AndDistance(Vector3 _point, float _distance)
    {
        point = _point;
        distance = _distance;
    }
}
