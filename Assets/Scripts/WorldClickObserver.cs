using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

public class WorldClickObserver : MonoBehaviour, IMixedRealityPointerHandler
{
    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        Main.instance.GlobalClick();
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        Main.instance.GlobalClick();
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
       
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        
    }
}
